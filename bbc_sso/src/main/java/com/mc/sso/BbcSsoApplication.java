package com.mc.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbcSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BbcSsoApplication.class, args);
    }

}

