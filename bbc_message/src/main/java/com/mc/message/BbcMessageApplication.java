package com.mc.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbcMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(BbcMessageApplication.class, args);
    }

}

