package com.mc.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbcProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(BbcProductApplication.class, args);
    }

}

