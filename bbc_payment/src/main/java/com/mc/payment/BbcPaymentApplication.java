package com.mc.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbcPaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(BbcPaymentApplication.class, args);
    }

}

