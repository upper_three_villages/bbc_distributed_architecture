package com.mc.order.common.utils;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * OkHttp 封装
 *
 * @author zhaojinqiang
 */
@Slf4j
public class OkHttpUtil {

    private static final OkHttpClient OK_HTTP_CLIENT;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    static {
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor(new HttpLogger());
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OK_HTTP_CLIENT = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(logInterceptor)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    /**
     * get请求，不带请求参数及Header
     *
     * @param url 请求url
     * @author zhaojq
     * @date 2019/1/8
     */
    public static OkHttpResult get(String url) {
        return get(url, null);
    }

    /**
     * get请求，带请求参数
     *
     * @param url         请求url
     * @param queryParams url请求参数
     * @return OkHttpResult
     * @author zhaojq
     * @date 2019/1/8
     */
    public static OkHttpResult get(String url, Map<String, String> queryParams) {
        return get(url, queryParams, null);
    }

    /**
     * get请求，带请求参数及Header
     *
     * @param url         请求url
     * @param queryParams url请求参数
     * @param headerMap   Header
     * @return OkHttpResult
     * @author zhaojq
     * @date 2019/1/8
     */
    public static OkHttpResult get(String url, Map<String, String> queryParams, Map<String, String> headerMap) {
        try {
            Request.Builder reqBuild = new Request.Builder();
            HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();

            //添加查询参数
            if (queryParams != null) {
                queryParams.forEach((key, value) -> {
                    if (value == null) {
                        value = "";
                    }
                    urlBuilder.addQueryParameter(key, value);
                });
            }

            //添加header
            if (headerMap != null) {
                headerMap.forEach((key, value) -> {
                    if (value == null) {
                        value = "";
                    }
                    reqBuild.header(key, value);
                });
            }

            Request request = reqBuild.url(urlBuilder.build()).build();
            Response response = OK_HTTP_CLIENT.newCall(request).execute();
            return new OkHttpResult(response);
        } catch (IOException exception) {
            log.error("", exception);
            return new OkHttpResult("GET请求发生异常");
        }
    }

    /**
     * post请求，带请求参数(x-www-form-urlencoded)
     *
     * @param url         请求url
     * @param queryParams 请求参数
     * @return OkHttpResult
     * @author zhaojq
     * @date 2019/1/8
     */
    public static OkHttpResult post(String url, Map<String, Object> queryParams) {
        return post(url, queryParams, null);
    }

    /**
     * post请求，带请求参数及Header (x-www-form-urlencoded)
     *
     * @param url         请求url
     * @param queryParams 请求参数
     * @param headerMap   请求Header
     * @return OkHttpResult
     * @author zhaojq
     * @date 2019/1/8
     */
    public static OkHttpResult post(String url, Map<String, Object> queryParams, Map<String, String> headerMap) {
        return post(url, null, queryParams, headerMap);
    }

    /**
     * post请求，带请求参数 (application/json)
     *
     * @param url       请求url
     * @param jsonValue 请求参数
     * @return OkHttpResult
     * @author zhaojq
     * @date 2019/1/8
     */
    public static OkHttpResult post(String url, String jsonValue) {
        return post(url, jsonValue, null, null);
    }

    /**
     * post请求，带请求参数及Header (application/json)
     *
     * @param url       请求url
     * @param jsonValue 请求参数
     * @param headerMap 请求Header
     * @return OkHttpResult
     * @author zhaojq
     * @date 2019/1/8
     */
    public static OkHttpResult post(String url, String jsonValue, Map<String, String> headerMap) {
        return post(url, jsonValue, null, headerMap);
    }

    /**
     * POST Http请求
     *
     * @param url         请求url
     * @param json        json数据
     * @param queryParams 表单数据
     * @param headerMap   header
     * @return OkHttpResult
     */
    private static OkHttpResult post(String url, String json, Map<String, Object> queryParams, Map<String, String> headerMap) {

        try {
            RequestBody requestBody = null;

            //json请求数据
            if (json != null) {
                requestBody = RequestBody.create(JSON, json);
            }

            //普通表单请求数据 x-www-form-urlencoded
            if (queryParams != null) {
                FormBody.Builder builder = new FormBody.Builder();
                queryParams.forEach((key, value) -> {
                    if (value == null) {
                        value = "";
                    }
                    builder.add(key, value.toString());
                });
                requestBody = builder.build();
            }

            Request.Builder reqBuilder = new Request.Builder();
            //添加header
            if (headerMap != null) {
                headerMap.forEach((key, value) -> {
                    if (value == null) {
                        value = "";
                    }
                    reqBuilder.header(key, value);
                });
            }
            Request request = reqBuilder.url(url).post(requestBody).build();
            Response response = OK_HTTP_CLIENT.newCall(request).execute();
            return new OkHttpResult(response);
        } catch (IOException exception) {
            log.error("", exception);
            return new OkHttpResult("POST请求发生异常");
        }
    }

    public static class OkHttpResult {

        public OkHttpResult(Response response) throws IOException {
            if (response != null) {
                this.isSuccess = response.isSuccessful();
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    this.result = responseBody.string();
                }
                this.httpRespCode = response.code();
            }
        }

        public OkHttpResult(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        /**
         * 是否成功
         */
        private boolean isSuccess;
        /**
         * 错误消息
         */
        private String errorMessage;
        /**
         * 请求结果
         */
        private String result;

        /**
         * Http 响应code
         */
        private Integer httpRespCode;

        public boolean isSuccess() {
            return isSuccess;
        }

        public void setSuccess(boolean success) {
            isSuccess = success;
        }

        public String getErrorMessage() {
            if(errorMessage == null){
                return "接口请求失败，返回值" + httpRespCode;
            }
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public Integer getHttpRespCode() {
            return httpRespCode;
        }

        public void setHttpRespCode(Integer httpRespCode) {
            this.httpRespCode = httpRespCode;
        }
    }

    private static class HttpLogger implements HttpLoggingInterceptor.Logger {

        @Override
        public void log(String s) {
            log.info(s);
        }
    }
}
