package com.mc.order.common.config;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.domain.proto.storage.DownloadByteArray;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.io.*;

@Configuration
public class FastDFSConfig {

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    /**
     * 上传
     * @author lucifer
     * @date 2019/4/16 17:08
     */
    public String upload(MultipartFile file) throws Exception {
        StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(),
                getFileExt(file.getName()), null);
        return storePath.getFullPath();
    }

    /**
     * 上传
     * @author lucifer
     * @date 2019/4/16 17:08
     */
    public String upload(File file) throws Exception {
        MultipartFile multipartFile = new MockMultipartFile(file.getName(), new FileInputStream(file));
        return upload(multipartFile);
    }

    /**
     * 上传
     * @author lucifer
     * @date 2019/4/16 17:08
     */
    public String upload(String fileBase64) throws Exception {
        String fileFormat = fileBase64.split(";")[0].split(":")[1];
        String fileExtName;
        switch (fileFormat.toLowerCase()) {
            case "image/gif":
                fileExtName = "gif";
                break;
            case "image/png":
                fileExtName = "png";
                break;
            case "image/jpeg":
                fileExtName = "jpg";
                break;
            case "image/x-icon":
                fileExtName = "icon";
                break;
            default:
                fileExtName = "txt";
                break;
        }
        byte[] imgByte = new BASE64Decoder().decodeBuffer(fileBase64.substring(fileBase64.indexOf(",", 0) + 1));
        File imageFile = base64ToFile(fileBase64, imgByte);
        StorePath storePath = fastFileStorageClient.uploadFile(new ByteArrayInputStream(imgByte), imageFile.length(),
                fileExtName, null);
        return storePath.getFullPath();
    }

    /**
     * 获取文件后缀名
     *
     * @param fileName
     * @return 如："jpg"、"txt"、"zip" 等
     */
    private static String getFileExt(String fileName) {
        if (StringUtils.isBlank(fileName) || !fileName.contains(".")) {
            return "";
        } else {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        }
    }

    /**
     * base64转文件
     * @author lucifer
     * @date 2019/4/16 17:08
     */
    public static File base64ToFile(String base64, byte[] buff) {
        if (base64 == null || "".equals(base64)) {
            return null;
        }
        File file = null;
        FileOutputStream fileOutputStream = null;
        try {
            file = File.createTempFile("tmp", null);
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(buff);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    /**
     * 删除
     * @author lucifer
     * @date 2019/4/16 17:07
     */
    public void delete(String fileId) {
        fastFileStorageClient.deleteFile(fileId);
    }

    /**
     * 下载文件
     * @author lucifer
     * @date 2019/4/3 16:48
     */
    public InputStream download(String fileUrl){
        String group = fileUrl.substring(0, fileUrl.indexOf("/"));
        String path = fileUrl.substring(fileUrl.indexOf("/") + 1);
        DownloadByteArray downloadByteArray = new DownloadByteArray();
        byte[] bytes = fastFileStorageClient.downloadFile(group, path, downloadByteArray);
        InputStream inputStream = new ByteArrayInputStream(bytes);
        return inputStream;
    }
}
