package com.mc.order.modules.order.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

@RefreshScope
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private RedisLockRegistry redisLockRegistry;
    @RequestMapping("/test")
    public void test(){
        Lock lock=redisLockRegistry.obtain("lock");
        try {
            Boolean b1=lock.tryLock(3, TimeUnit.SECONDS);

            log.info("b1 is : {}", b1);
            TimeUnit.SECONDS.sleep(5);
            if(b1){
                try {

                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
