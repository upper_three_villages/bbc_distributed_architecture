package com.mc.order.modules.upload.controller;

import com.mc.order.common.config.FastDFSConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("file")
@RestController
public class FileUploadController {
    @Autowired
    private FastDFSConfig fastDFSConfig;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "upload")
    public String upload(MultipartFile file) throws Exception {

        return fastDFSConfig.upload(file);

    }
}
