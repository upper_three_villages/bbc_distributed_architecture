package com.mc.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.jmx.support.RegistrationPolicy;

@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.mc"})
public class BbcOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(BbcOrderApplication.class, args);
    }

}

