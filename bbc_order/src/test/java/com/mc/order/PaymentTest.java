package com.mc.order;

import com.mc.order.common.utils.MD5Util;
import com.mc.order.common.utils.OkHttpUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class PaymentTest {
    private String buildSign(Map<String, String> input, String key) {
        String inputStr = input.entrySet().stream()
                .filter(o -> !StringUtils.isEmpty(o.getValue()) && !o.getKey().equals("sign_type"))
                .sorted(Comparator.comparing(Map.Entry::getKey)).map(o -> String.format("%s=%s", o.getKey(), o.getValue()))
                .collect(Collectors.joining("&"));
        String stringSignTemp = String.format("%s%s", inputStr, key);
        return MD5Util.encodeByMD5(stringSignTemp, "UTF-8");
    }

    private Map<String, String> buildRequest(Map<String, String> input, String key) {
        input.put("_input_charset", "UTF-8");
        input.put("sign_type", "MD5");
        input.put("version", "1.0");
        input.put("sign", this.buildSign(input, key));
        input = input.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toMap(Map.Entry::getKey,
                Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        return input;
    }

    public static String url = "https://mag.superpayglobal.com/mag/gateway/receiveOrder.do";
    public static String key = "456";

    @Test
    public void searchAccountDetailTest() {
        Map<String, String> input = new HashMap<String, String>() {
            private static final long serialVersionUID = 1L;
            {
                put("account_type", "203");
                put("buyer_ip", "102.122.12.45");
                put("identity_no", "201548863698");
                put("identity_type", "MEMBER_ID");
                put("partner_id", "188888888888");
                put("request_no", "2019013111015455538693829905");
                put("return_url", "http://localhost:8112/tpu/mag/syncNotify.do");
                put("service", "query_account_detail");
            }
        };
        OkHttpUtil.OkHttpResult okHttpResult = OkHttpUtil.get(url, this.buildRequest(input, key));
        if (okHttpResult.isSuccess()) {
            Assert.assertTrue(okHttpResult.getResult().contains("account_balance_list"));
        }
    }

    @Test
    public void offlineRechargeTest() {
        Map<String, String> input = new HashMap<String, String>() {
            private static final long serialVersionUID = 1L;
            {
                put("amount", "0.04");
                put("buyer_ip", "102.122.12.45");
                put("identity_no", "201548863698");
                put("identity_type", "MEMBER_ID");
                put("partner_id", "188888888888");
                put("request_no", "2019013111023563370423634807");
                put("service", "offline_deposit");
            }
        };
        OkHttpUtil.OkHttpResult okHttpResult = OkHttpUtil.get(url, this.buildRequest(input, key));
        if (okHttpResult.isSuccess()) {
            Assert.assertTrue(okHttpResult.getResult().contains("\"is_success\":\"T\""));
        }
    }

    @Test
    public void securiedTransactionTest() {
        Map<String, String> input = new HashMap<String, String>() {
            private static final long serialVersionUID = 1L;
            {
                put("buyer_id", "201548863698");
                put("buyer_id_type", "MEMBER_ID");
                put("buyer_ip", "102.112.12.45");
                put("buyer_mobile", "13812345678");
                put("go_cashier", "Y");
                put("identityType2", "MEMBER_ID");
                put("is_anonymous", "N");
                put("is_web_access", "Y");
                put("operator_id", "10005454");
                put("partner_id", "188888888888");
                put("request_no", "2019013111162524937727581334");
                put("return_url", "http://localhost:8112/tpu/mag/syncNotify.do");
                put("service", "create_ensure_trade");
                put("trade_list",
                        "28:2019013111162443964868152678~5:东风雪铁龙~4:0.10~1:1~4:0.10~4:0.05~12:201548860040~9:MEMBER_ID~11:13855462942~13:爱丽舍-三厢 1.6 MT~35:http://www.test.com/?product-9.html~14:20140526090530~43:http://localhost:8112/tpu/mag/asynNotify.do~2:7d~7:上海大众4S店");
            }
        };
        OkHttpUtil.OkHttpResult okHttpResult = OkHttpUtil.get(url, this.buildRequest(input, key));
        if (okHttpResult.isSuccess()) {
            Assert.assertTrue(okHttpResult.getResult().length() > 0);
        }
    }

    @Test
    public void confirmReceiptTest() {
        Map<String, String> input = new HashMap<String, String>() {
            private static final long serialVersionUID = 1L;
            {
                put("operator_id", "10005454");
                put("outer_trade_no", "2019013111162443964868152678");
                put("partner_id", "188888888888");
                put("request_no", "2019013111184189359596795159");
                put("return_url", "http://localhost:8112/tpu/mag/syncNotify.do");
                put("royalty_parameters", "0:");
                put("service", "create_settle");
            }
        };
        OkHttpUtil.OkHttpResult okHttpResult = OkHttpUtil.get(url, this.buildRequest(input, key));
        if (okHttpResult.isSuccess()) {
            Assert.assertTrue(okHttpResult.getResult().length() > 0);
        }
    }
}
