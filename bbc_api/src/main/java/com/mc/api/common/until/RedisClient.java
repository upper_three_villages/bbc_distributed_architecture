package com.mc.api.common.until;

import com.mc.api.common.constant.RedisKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisClient {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 设置有效期
     * @author lucifer
     * @date 2018/12/24 17:36
     */
    public Boolean expire(RedisKeyEnum key, int seconds) {
        if (seconds < 0) {
            return false;
        }
        return redisTemplate.expire(key.getKey(), seconds, TimeUnit.SECONDS);
    }

    /**
     * 获取匹配的所有key
     * @author lucifer
     * @date 2018/12/24 17:23
     */
    public Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * 删除keys对应的记录,可以是多个key
     * @return 删除的记录数
     * @author lucifer
     * @date 2018/12/24 17:23
     */
    public long del(String... keys) {
        List<String> keysList = new ArrayList<>();
        keysList.addAll(Arrays.asList(keys));
        return redisTemplate.delete(keysList);
    }

    /**
     * 返回指定key存储的类型
     * @return String string|list|set|zset|hash
     * @author lucifer
     * @date 2018/12/24 17:24
     */
    public String type(RedisKeyEnum key) {
        DataType dataType = redisTemplate.type(key.getKey());
        if (dataType != null) {
            return dataType.code();
        }
        return null;
    }

    /**
     * 根据key获取记录
     * @author lucifer
     * @date 2018/12/24 17:28
     */
    public Object get(RedisKeyEnum key) {
        return redisTemplate.opsForValue().get(key.getKey());
    }

    /**
     * 添加一条记录，仅当给定的key不存在时才插入
     * @author lucifer
     * @date 2018/12/24 17:25
     */
    public Boolean setnx(RedisKeyEnum key, String value) {
        return redisTemplate.opsForValue().setIfAbsent(key.getKey(), value);
    }

    /**
     * 添加记录,如果记录已存在将覆盖原有的value
     * @author lucifer
     * @date 2018/12/24 17:29
     */
    public void set(RedisKeyEnum key, String value) {
        redisTemplate.opsForValue().set(key.getKey(), value);
    }
    /**
     * hash中指定的存储是否存在
     * @author lucifer
     * @date 2018/12/24 17:26
     */
    public boolean hexists(RedisKeyEnum key, String field) {
        return redisTemplate.opsForHash().hasKey(key.getKey(), field);
    }

    /**
     * hash中添加记录
     * @author lucifer
     * @date 2018/12/24 17:27
     */
    public void hset(RedisKeyEnum key, String field, Object value) {
        redisTemplate.opsForHash().put(key.getKey(), field, value);
    }

    /**
     * 获取hash中key对应的值
     * @author lucifer
     * @date 2018/12/24 17:27
     */
    public Object hget(RedisKeyEnum key, String field) {
        return redisTemplate.opsForHash().get(key.getKey(), field);
    }

    /**
     * 删除hash中指定的字段
     * @author lucifer
     * @date 2018/12/24 17:29
     */
    public long hdel(RedisKeyEnum key, String field) {
        return redisTemplate.opsForHash().delete(key.getKey(), field);
    }
}
