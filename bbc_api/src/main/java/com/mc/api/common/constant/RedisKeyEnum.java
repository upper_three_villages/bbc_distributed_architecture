package com.mc.api.common.constant;

/**
 * redis key
 * @author lucifer
 * @date 2018/12/24 17:47
 */
public enum RedisKeyEnum {
    ORDER_NUM("order_num");



    private String key;
    RedisKeyEnum(String key){
        this.key=key;
    }
    public String getKey(){
        return key;
    }
}
