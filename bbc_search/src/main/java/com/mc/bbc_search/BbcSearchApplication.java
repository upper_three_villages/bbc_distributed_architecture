package com.mc.bbc_search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbcSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(BbcSearchApplication.class, args);
    }

}

